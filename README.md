# Shopping Robot

A [Selenium](https://www.seleniumhq.org/) project to promote products on Amazon.

### Prepare the code
  - Install [GIT](https://git-scm.com/downloads)
  - Install [Python 3.7.4](https://www.python.org/downloads/release/python-374/). 
    - Do not forget to add python on your PATH.
    - Other versions of Python 3 can be used.
  - Download the code:
    - Open a terminal, select a working directory and type:
        ```
        $ git clone https://gitlab.com/igorcouto/shoppingrobot
        ```
    - A folder called amazonbot will be created with all project files. Enter on the folder:
        ```
        $ cd shoppingrobot
        ```
    - Install the project dependencies:
        ```
        $ pip install -r requirements.txt
        ```
  - Install [Google Chrome](https://www.google.com/chrome).
  - Download [ChromeDriver](http://chromedriver.chromium.org/downloads) executables according to your Google Chrome version.
    - On the project folder, creates a folder called *chromedriver*. Put the chromedriver executable there.
  - The code is ready!

### Settings
Before to run the robot, please setup:
  - Always verify if a new version of the code has been released:
    ```
    $ git pull
    ```
  - On *settings/robot.ini*, edit variables according to your will.
    - MONGODB section holds variables related to access data in the cloud.
    - FOLDERS section have two variables:
        - temp: where robot will unzip userdata directories.
        - zipped_userdata: where you keep the zipped userdata directories.
  - To keep userdata directories updated, use *compress_userdata.py* in recursive mode. Every time the robot finishes to use an userdata directory, compress_userdata will zip the directory and save it in a destination folder. Open a second terminal and run:
    ```
    $ python src\compress_userdata.py -r -source <TEMP_FOLDER> -destination <ZIPPED_USERDATA_FOLDER>
    ```
  - On the *data* folder, edit *products.csv* to inform which products will be promoted.

### Usage
  - To execute an instance of the robot, you always need to provide a control file - a basic CSV table. From this file, the robot will retrieve which actions need to be executed in a customer account. To get a template of this file, please see *data/control_file.csv*. On command line prompt, go to root project folder and type:
      ```
      $ python src\start.py <CONTROL_FILE>
      ```
  - To use a proxy service chooses between Microleaves (ml) and NetNut (nn).
      ```
      $ python src\start.py -proxy ml
      ```
  - By default, the robot goes to the zipped_userdata folder and chooses a customer based on zipped files. If you wish select some customers, create a file like *data/customers.csv* and pass it to the robot as an argument.
      ```
      $ python src\start.py -c data/customers.csv
      ```