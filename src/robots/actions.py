import sys
import inflect
from common import errors
from os.path import abspath, join
from random import choice, randint
from datetime import datetime as dt
from selenium.webdriver.common.by import By
from resources.mailbox import wait_for_code
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from common.utils import random_sleep, get_logger, update_product
from selenium.common.exceptions import TimeoutException, NoSuchElementException


def get_status(elmt):
    """Search for "Sponsored" text inside an element.

    Parameters
    ----------
    elmt : str
        The status of element (sponsored or not).
    """
    status = 'non_sponsored'
    try:
        elmt.find_element_by_xpath('.//div[@data-component-type="sp-sponsored-result"]')
        status = 'sponsored'
    except NoSuchElementException:
        pass

    return status

def search_elmt_by_asin(listing, asins):
    """Searches for ASIN code inside an listing element.


    Parameters
    ----------
    listing : selenium.webdriver.remote.webelement.WebElement
        The WebElement to search inside.
    asins : list
        The ASIN codes that will be search.
    
    Return
    ------
    found_elmt : list
        The found elements.
    """
    found_elmt = []
    elmts = listing.find_elements_by_xpath('.//*[@data-asin]')
    for elmt in elmts:
        asin = elmt.get_attribute('data-asin')
        if asin in asins:
            found_elmt.append(elmt)
    
    return found_elmt

class Actions():
    def __init__(self, driver, settings):
        self.driver = driver
        self.settings = settings
        self.wait = WebDriverWait(self.driver,
                                  int(self.settings['DRIVER']['wait_time']))  

    def save_screenshot(self, filename):
        """Saves a screenshot of browser page on temp folder.
        """
        # Screenshot
        temp_folder = abspath(self.settings['FOLDERS']['temp'])
        screenshot_file = join(temp_folder, filename) + '.png'
        self.driver.save_screenshot(screenshot_file)

    def is_logged_in(self):
        """Check if the customer is logged in.

        Return
        ------
        logged_in : bool
            If True, the customer is logged in.
        """
        # Creates a logger
        logger = get_logger('is_logged_in')
        try:
            nav_link_accountList_elmt = self.driver.find_element_by_id('nav-link-accountList')
            nav_link_accountList_elmt.find_element_by_xpath('.//span[text()="Hello, Sign in"]')
        except NoSuchElementException:
            logger.info('Logged in.')
            return True
        
        logger.info('Not logged in.')
        return False

    def go_to_initial_page(self):
        """Goes to initial page.
        """
        self.driver.get(self.settings['DRIVER']['initial_page'])
        random_sleep() # zZzZz...
        self.driver.refresh()
        
    def open_login_page(self):
        """Only goes to the login page.

        Return
        ------
        success : bool
            If True, the driver open the login page.
        """
        # Try to click on "Signin" floating button
        try:
            nav_signin_tooltip_elmt = WebDriverWait(self.driver, 2).until(
                EC.visibility_of_element_located((By.ID, 'nav-signin-tooltip'))
            )
            nav_signin_tooltip_elmt.find_element_by_xpath('//a[@data-nav-role="signin"]').click()
            random_sleep() # zZzZz...
            return True
        except:
            pass
        # Try to click on nav-tools bar
        try:
            self.driver.find_element_by_id('nav-link-accountList').click()
            random_sleep() # zZzZz...
            return True
        except TimeoutException:
            pass
   
    def fill_login_form(self):
        """Fills the login form with customer information.
        """
        self.driver.find_element_by_id('ap_email').send_keys(self.settings['customer_info']['email'])
        random_sleep(min=1, max=3) # zZzZz...
        self.driver.find_element_by_id('continue').click()
        try:
            WebDriverWait(self.driver, 2).until(
                EC.visibility_of_element_located((By.ID, 'auth-error-message-box'))
            )
            self.quit()
            raise errors.AccountProblemError(self.settings['customer_info']['email'])
        except TimeoutException:
            pass

        random_sleep(min=1, max=3) # zZzZz...
        self.driver.find_element_by_id('ap_password').send_keys(self.settings['customer_info']['password'])
        self.driver.find_element_by_xpath('//input[@name="rememberMe"]').click()
        try:
            WebDriverWait(self.driver, 2).until(
                EC.visibility_of_element_located((By.ID, 'auth-warning-message-box'))
            )
            self.quit()
            raise errors.AccountProblemError(self.settings['customer_info']['email'])
        except TimeoutException:
            pass

        random_sleep(min=1, max=3) # zZzZz...
        self.driver.find_element_by_id('signInSubmit').click()
        random_sleep(min=1, max=3) # zZzZz...

    def verification_needed(self):
        """Verifies if Amazon shows a verification page. And clicks on "Continue"
        or "Send Code" button.

        Return
        ------
        success : bool
            If True, Amazon took the robot to verification page. Otherwise,
            go shopping.
        """
        # Creates a logger
        logger = get_logger('verification_needed')
        # Verification needed?
        try:
            verify_form_xpath = '//form[@name="claimspicker" and @action="verify"]'
            self.wait.until(lambda d: d.find_element_by_xpath(verify_form_xpath))
            logger.warning('Verification needed.')
            random_sleep(min=1, max=2) # zZzZz...
            self.wait.until(lambda d: d.find_element_by_id('continue')).click()
            return True
        except TimeoutException:
            return False

    def enter_the_code(self, code):
        """Enters the verification code.
        
        Parameters
        ----------
        code : str
            The code sent to customer email.
        """
        self.driver.find_element_by_xpath('//input[@name="code"]').send_keys(code)
        random_sleep(min=1, max=2) # zZzZz...
        self.driver.find_element_by_xpath('//input[@type="submit"]').click()

    def exist_error_msgbox(self):
        """Checks if exists a authentication error message box.
        """
        logger = get_logger('exist_error_msgbox')
        try:
            elmt_auth_error_message_box = self.wait.until(
                lambda d: d.find_element_by_id('auth-error-message-box')
            )
            # Gets the error message
            error_msg = (elmt_auth_error_message_box.text).replace('\n', '. ')
            # Saves a screenshot of page
            self.save_screenshot(self.settings['customer_info']['email'])
            # Close the browser
            self.quit()
            raise errors.AccountProblemError(self.settings['customer_info']['email'], msg=error_msg)
        except TimeoutException:
            pass

    def exist_warn_msgbox(self):
        logger = get_logger('exist_warn_msgbox')
        try:
            elmt_auth_warn_message_box = self.wait.until(
                lambda d: d.find_element_by_id('auth-warning-message-box')
            )
            # Gets the warning message
            warn_msg = (elmt_auth_warn_message_box.text).replace('\n', '. ')
            # Saves a screenshot of page
            self.save_screenshot(self.settings['customer_info']['email'])
            # Close the browser
            self.quit()
            raise errors.AccountProblemError(self.settings['customer_info']['email'], msg=warn_msg)
        except TimeoutException:
            pass

    def empty_cart(self):
        """Emptys the cart.
        """
        # Creates a logger
        logger = get_logger('empty_cart')

        try:
            cart_count = self.wait.until(lambda d: d.find_element_by_id('nav-cart-count')).text
            if cart_count != '0':
                infl = inflect.engine()
                logger.info('%s %s found on cart.' % (cart_count,
                                                      infl.plural('item',cart_count)))
                self.wait.until(lambda d: d.find_element_by_id('nav-cart')).click()
                self.wait.until(lambda d: d.find_element_by_id('sc-active-cart'))

                while cart_count != '0':
                    products = self.wait.until(lambda d: d.find_elements_by_xpath('//*[@data-asin and not(@data-removed)]'))
                    product = products.pop()
                    asin = product.get_attribute('data-asin')
                    product.find_element_by_xpath('.//input[@value="Delete"]').click()
                    WebDriverWait(self.driver, int(self.settings['DRIVER']['wait_time'])).until(
                        EC.presence_of_element_located((By.XPATH,
                                                        '//*[@data-asin="%s" and @data-removed]' % asin))
                    )
                    cart_count = self.wait.until(lambda d: d.find_element_by_id('nav-cart-count')).text
                    logger.info('%s removed from cart.' % asin)

            else:
                logger.info('No item found on cart.')
        except TimeoutException:
            pass

    def type_on_search_bar(self, keyword):
        """Types a keyword on search bar.
        
        Parameters
        ----------
        keyword : str
            Keyword to be typed in searchbar
        """
        # Creates a logger
        logger = get_logger('type_on_search_bar')

        logger.info('KEYWORD: %s' % keyword)
        # Locates the search bar.
        search_bar = self.wait.until(lambda d: d.find_element_by_id('twotabsearchtextbox'))
        search_bar.clear()
        search_bar.send_keys(keyword)
        random_sleep(min=1, max=2) # zZzZz...
        # Clicks in "Go"
        try:
            self.wait.until(lambda d: d.find_element_by_xpath(
                '//*[@id="nav-search-submit-text"]/following-sibling::input'
            )).click()
        except TimeoutException:
            pass

        random_sleep(min=2, max=4) # zZzZz...

    def click_on_next(self):
        """Clicks on next button
        """
        try:
            self.wait.until(lambda d: d.find_element_by_xpath('//li[@class="a-last"]/a')).click()
            return True
        except TimeoutException:
            return False

    def search_asins(self, asins, page_limit=21):
        """Searches asins on listing. If a asin is found, clicks on product image.

        Parameters
        ----------
        asins : list
            The list with ASIN codes.
        page_limit : int
            The maximum number of pages that robot can access.
        """
        logger = get_logger('search_asins')
        logger.info('ASIN: %s' % asins)
        pos_on_listing = 1
        for npage in range(1, page_limit + 1):
            # Filtering elements with products links
            listing = self.driver.find_element_by_xpath('.//*[@data-component-type="s-search-results"]')
            products = listing.find_elements_by_xpath('.//*[@data-asin]')
            for pos, elmt in enumerate(products, 1):
                # Gather product information
                elmt_asin = elmt.get_attribute('data-asin')
                elmt_title = elmt.find_element_by_xpath(
                    './/span[@data-component-type="s-product-image"]//img'
                ).get_attribute('alt')
                elmt_status = get_status(elmt)
                #logger.info('\t'.join([str(pos_on_listing), elmt_asin, elmt_status, elmt_title[:30]]))

                if elmt_asin in asins:
                    logger.info('%s found on page %s in position %s.' % (elmt_asin, npage, pos_on_listing))
                    # Filter products by status (sponsored, non sponsored or any).
                    status = (self.settings.get('customer_info')).get('product_status')
                    if not status or elmt_status == status:
                        # Clicks on product image
                        elmt.find_element_by_xpath('.//*[@data-component-type="s-product-image"]/a').click()
                        return True
                    else:
                        logger.info('Yet a %s product is being looked for.' % status)

                # Controls the position of the product on listing
                pos_on_listing+=1

            # The page limit is reached
            if page_limit == npage:
                logger.info('Page limit (%s) reached.' % page_limit)
                break

            # Go to next page.
            if not self.click_on_next():
                random_sleep(min=4, max=7) # zZzZz...
                logger.info('Last page of the listing (%s) reached. Please choose another keyword.' % npage)
                break
        
        return False

    def see_images(self):
        """Clicks on every thumbnail image of the product. Browser instance must in
        a product page to works.
        """
        # Creates a logger
        logger = get_logger('see_images')
        try:
            tumbimages = self.wait.until(
                lambda d: d.find_element_by_id('altImages')
            ).find_elements_by_xpath('//li[contains(@class, "imageThumbnail")]')
            # Log how many tumbnail images product has
            logger.info('Analyzing %s product images' % len(tumbimages))
            # Up to down or reversed
            if choice((True, False)):
                tumbimages.reverse()
            # Browser for all of them
            for img in tumbimages:
                random_sleep(min=1, max=2) # zZzZzZz...
                img.find_element_by_tag_name('input').click()
        except Exception:
            logger.info('No thumbnail images was found!')

    def see_and_choose_a_color(self):
        """Clicks on every color variation of the product. Browser instance must in a
        product page to works.
        """
        # Creates a logger
        logger = get_logger('see_and_choose_a_color')
        try:
            # Count how many color variations product has
            color_variations = self.wait.until(
                lambda d: d.find_element_by_id('variation_color_name')
            ).find_elements_by_xpath('//li[starts-with(@id, "color_name")]')
            logger.info('Analyzing %s color variations' % len(color_variations))
            
            # Left to right or reversed
            if choice((True, False)):
                color_variations.reverse()
            # Browser for all of them
            for color in color_variations:
                random_sleep() # zZzZzZz...
                color.click()
            random_sleep() # zZzZzZz...
            # Choose a color
            chosen_color = choice(color_variations)
            chosen_asin = chosen_color.get_attribute('data-defaultasin')
            logger.info('Product ASIN chosen: %s' % chosen_asin)
            chosen_color.click()
            random_sleep() # zZzZzZz...
        except Exception:
            logger.warning('No color variations found!')

    def see_popup_reviews(self):
        """Passes the mouse over the reviews popup. Browser instance must in a
        product page to works.
        """
        # Creates a logger
        logger = get_logger('see_popup_reviews')

        action = ActionChains(self.driver)
        
        try:
            reviews_popup = self.wait.until(lambda d: d.find_element_by_id('acrPopover'))
            action.move_to_element(reviews_popup).perform()
            logger.info('Seeing reviews popup')
            random_sleep(min=6) # zZzZzZz...
        except Exception:
            logger.info('Product does not have reviews yet.')
            pass

    def see_reviews(self):
        """Go to reviews section. The currentHeight and finalHeight returns
        can be used to scroll down and scroll up.
        
        Returns
        -------
        currentHeight : int
            Where reviews section begins
        finalHeight : int
            Where reviews section ends
        """
        # Creates a logger
        logger = get_logger('see_reviews')

        try:
            # Go to reviews
            self.wait.until(lambda d: d.find_element_by_id('acrCustomerReviewLink')).click()
            logger.info('See reviews section')
            # Get reviews and calculate the beginning and end of element
            reviews = self.wait.until(lambda d: d.find_element_by_id('reviewsMedley'))
            currentHeight = self.driver.execute_script("return window.pageYOffset")
            finalHeight = currentHeight + reviews.size['height']
            random_sleep() # zZzZzZz...
            return currentHeight, finalHeight

        except Exception:
            logger.info('Product does not have a review section yet.')
            return None, None

    def scroll_down(self, currentHeight = 0, finalHeight = 0):
        """Makes the browser scrolls down.

        Parameters
        ----------
        currentHeight : int
            Scroll movement starts from this value. If not set, will starts from position 0.
        finalHeight : int
            Scroll movement finish in this value. If not set, will finish on the botton of page.
        """
        # Creates a logger
        logger = get_logger('scroll_down')

        logger.info('Scrolling down')
        if not currentHeight:
            currentHeight = self.driver.execute_script("return window.pageYOffset")
        if not finalHeight:
            finalHeight = self.driver.execute_script("return document.body.scrollHeight")
        if currentHeight < finalHeight: # final is not bigger then current height
            step = int((finalHeight - currentHeight)/20)
            for height in range(currentHeight, finalHeight, step)[0:-1]:
                random_sleep(min=2, max=4) # zZzZzZz...
                for sub_height in range(height, height+step, 20):
                    self.driver.execute_script('window.scrollTo(0, %s);' % sub_height)

    def scroll_up(self, currentHeight = 0, finalHeight = 0):
        """Makes the browser scrolls up.

        Parameters
        ----------
        currentHeight : int
            Scroll movement starts from this value. If not set, will starts from the botton of page.
        finalHeight : int
            Scroll movement finish in this value. If not set, will finish on the top of page.
        """
        # Creates a logger
        logger = get_logger('scroll_up')

        logger.info('Scrolling up')
        if not currentHeight:
            currentHeight = self.driver.execute_script("return window.pageYOffset")
        if not finalHeight:
            finalHeight = self.driver.execute_script("return document.body.scrollTop")
        if currentHeight > finalHeight: # final is not smaller then current height
            step = int((currentHeight - finalHeight)/20)
            for height in reversed(range(finalHeight, currentHeight, step)[0:-1]):
                random_sleep(min=2, max=4) # zZzZzZz...
                for sub_height in reversed(range(height, height+step, 20)):
                    self.driver.execute_script('window.scrollTo(0, %s);' % sub_height)

    def choose_quantity(self):
        """Choose a quantity of product before insert in the cart. Browser
        instance must in a product page to works.
        """
        # Creates a logger
        logger = get_logger('choose_quantity')

        quantity = str(randint(1, 5))
        infl = inflect.engine()
    
        try:
            quantity_elmt = self.wait.until(lambda d: d.find_element_by_id('quantity'))
            select_qty = Select(quantity_elmt)
            select_qty.select_by_value(quantity)
            logger.info('Choosing %s %s.' % (quantity,
                                             infl.plural('item', quantity)))
                                                     
        except Exception:
            logger.warning('Is not possible select quantity.')
        
        random_sleep(min=6) # zZzZzZz...

    def add_to_cart(self):
        """Choose a quantity of product before insert in the cart. Browser
        instance must in a product page to works.

        Return
        ------
        success : bool
            If True, the button "Add to cart" is accessible.
        """
        # Creates a logger
        logger = get_logger('choose_quantity')

        try:
            self.wait.until(lambda d: d.find_element_by_id('add-to-cart-button')).click()
            logger.info('Adding to cart')
            random_sleep(min=6) # zZzZzZz...

            # Move the mouse to search bar to ignore some modals.
            action = ActionChains(self.driver)
            elmt_searchbox = self.driver.find_element_by_id('twotabsearchtextbox')
            action.move_to_element(elmt_searchbox).perform()
            action.click(elmt_searchbox).perform()
            
            return True
        except Exception:
            logger.info('"Add to cart" button did not appear.')
        
        return False

    def click_on_proceed_to_checkout(self):
        """Find and click on "Proceed to checkout" button."
        """
        # Creates a logger
        logger = get_logger('click_on_proceed_to_checkout')

        try:
            self.wait.until(lambda d: d.find_element_by_id('hlb-ptc-btn')).click()
            logger.info('Proceeding to checkout.')
        except Exception:
            pass
        
        # if a side panel appears, click on proceed.
        try:
            WebDriverWait(self.driver, 2).until(
                EC.visibility_of_element_located((By.XPATH,
                                                 '//*[@aria-labelledby="attach-sidesheet-checkout-button-announce"]'))
            ).click()
        except Exception:
            pass

        # If the site asked for password again
        try:    
            WebDriverWait(self.driver, 2).until(
                EC.presence_of_element_located((By.ID, 'ap_password'))
            ).send_keys(self.settings['customer_info']['password'])
            logger.info('Amazon asked for password again.')
            self.driver.find_element_by_xpath('//input[@name="rememberMe"]').click()
            random_sleep(min=1, max=3) # zZzZz...
            self.driver.find_element_by_id('signInSubmit').click()
            random_sleep(min=1, max=3) # zZzZz...
        except Exception:
            pass

    def find_shipping_address_form(self):
        """Only find if the shipping address form was reached.
        """
        # Creates a logger
        logger = get_logger('find_shipping_address_form')

        try:
            self.wait.until(lambda d: d.find_element_by_xpath(
                '//form[starts-with(@action, "/gp/buy/shipaddressselect")]'
            ))
            logger.info('Shipping address form was reached.')
            return True
        except Exception:
            pass

        return False

    def quit(self):
        # Creates a logger
        logger = get_logger('quit')
        logger.info('Closing the browser.')
        self.driver.quit()

    def run(self):
        # Creates a logger
        logger = get_logger('run')

        self.go_to_initial_page()
        if not self.is_logged_in():
            self.open_login_page()
            self.fill_login_form()
            # When Amazon wanna verifies
            if self.verification_needed():
                code = wait_for_code(username=self.settings['customer_info']['email'],
                                     password=self.settings['customer_info']['password'])
                self.enter_the_code(code)
                self.exist_error_msgbox()
                self.exist_warn_msgbox()
            
        # Empties the cart
        if self.settings['customer_info']['empty_cart']:
            self.empty_cart()
            self.go_to_initial_page()

        while True:
            # Search product if a keyword is informed
            if self.settings['keyword'] and self.settings['asins']:
                self.type_on_search_bar(self.settings['keyword'])
                search_status = self.search_asins(asins=self.settings['asins'],
                                                  page_limit=self.settings['page_limit'])
                
                if search_status:
                    self.see_images()
                    self.see_and_choose_a_color()
                    self.see_popup_reviews()
                    currentHeight, finalHeight = self.see_reviews()
                    self.scroll_down(currentHeight, finalHeight)
                    self.scroll_up()
                    self.choose_quantity()
                    if self.add_to_cart():
                        self.click_on_proceed_to_checkout()
                        if self.find_shipping_address_form():
                            logger.info('A complete iteration was reached.')
                            random_sleep(min=1, max=3) # zZzZz...   
                            self.quit()
                            return True
                        else:
                            self.quit()
                    else:
                        self.quit()
                    break
                else:
                    # Choose another product to search
                    self.settings = update_product(self.settings)