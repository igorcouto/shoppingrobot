from common.utils import get_logger
from robots.controller import launch_robot
from common.utils import update_customer, update_product
from selenium.common.exceptions import WebDriverException
from common.errors import (AccountProblemError, UserDataDirLockedError,
                           CustomerDataNotFoundOnCloudError)


def initialize(settings):
    """Initializes the entire process.

    Parameters
    ----------
    settings : dict
        A dictionary with all settings to initialize.
    """
    # Creates a logger
    logger = get_logger('initialize')

    total_iterations = settings.get('iterations')
    i = 1

    while i <= total_iterations:   
        try:
            print('\n---------- ITERATION %s ----------' % i)
            logger.info('A new iteration is starting')
            settings = update_customer(settings)
            settings = update_product(settings)
            successful = launch_robot(settings)
            if successful:
                i+=1
        except AccountProblemError:
            logger.info('Incomplete iteration. A new attempt will be made.')
        except UserDataDirLockedError:
            logger.info('Incomplete iteration. A new attempt will be made.')
        except WebDriverException as e:
            logger.error(e.msg)
            logger.info('Incomplete iteration. A new attempt will be made.')
        except CustomerDataNotFoundOnCloudError:
            logger.info('Incomplete iteration. A new attempt will be made.')

if __name__ == "__main__":
    pass