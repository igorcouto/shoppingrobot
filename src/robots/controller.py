import sys
from time import sleep
from selenium import webdriver
from random import randint, choice
from robots.actions import Actions
from os.path import abspath, basename, splitext
from common.ziptools import unzip_user_data_dir
from common.errors import ZippedUserDataNotFoundError
from common.utils import get_logger, is_original_driver, get_files

    
def create_driver(settings):
    """Creates a Selenium webdriver based on ChromeDriver.
    
    Parameters
    ----------
    settings : dict
        A dictionary with all settings to create a driver.
    Returns
    -------
    driver : webdriver.Chrome
        The ChromeDriver instance
    """
    # Creates a logger
    logger = get_logger('create_driver')

    opt = webdriver.ChromeOptions()
    # Setup the USER_AGENT
    browser_settings = settings.get('customer_info')
    if browser_settings:
        opt.add_argument('user-agent=%s' % browser_settings['user_agent'])
        opt.add_argument('window-size=%s' % browser_settings['resolution'])
        opt.add_argument('--lang=%s' % browser_settings['languages'])
    
    # User-data directory
    temp_folder = abspath(settings['FOLDERS']['temp'])
    opt.add_argument('user-data-dir=%s/%s' % (temp_folder,
                                              settings['customer_info']['email']))

    # Detach the driver from terminal
    if settings.get('detach'):
        logger.info('Detaching driver from terminal.')
        opt.add_experimental_option("detach", settings.get('detach'))
    
    # Adding extensions
    extensions = settings.get('EXTENSIONS')
    if extensions:
        for ext_name, ext_path in extensions.items():
            logger.info('Adding %s extension on driver.' % ext_name)
            opt.add_extension(ext_path)
    
    # Silent mode
    opt.add_argument("--log-level=3")

    if settings.get('proxy_service') == 'ml':
        logger.info('Microleaves PROXY service enabled.')
        if settings.get('proxy_server_ip'):
            server = settings.get('proxy_server_ip')
        else:
            server = '62.210.169.169'

        if settings.get('proxy_server_port'):
            port = settings.get('proxy_server_port')
        else:
            port = 3266 + randint(0, 7)
        
        proxy_host = '%s:%s' % (server, port)
        # Adding on ChromeOption object
        opt.add_argument('--proxy-server=%s' % proxy_host)
    
    # NetNut proxy service. Authentication needed.
    if settings.get('proxy_service') == 'nn':
        logger.info('NetNut PROXY service enabled.')
        proxy_host = 'us-s%s.netnut.io:33128' % randint(1, 380)
        # Adding on ChromeOption object
        opt.add_argument('--proxy-server=%s' % proxy_host)
        # Adding extension to authenticate automatically
        logger.info('Adding proxy_auth extension on driver')
        opt.add_extension(settings['NETNUT']['extension'])

    # Removing "Chrome is being controlled..."
    logger.info('"enable-automation" switch disabled.')
    opt.add_experimental_option("excludeSwitches", ['enable-automation'])
    opt.add_experimental_option('useAutomationExtension', False)

    opt.add_experimental_option('prefs', {
        'credentials_enable_service': False,
        'profile': {
            'password_manager_enabled': False
        }
    })

    # Finally creates the browser
    logger.info('Initializing the browser.')
    try:
        driver = webdriver.Chrome(executable_path=settings['DRIVER']['executable_path'],
                                  options=opt)
        print('')
    except FileNotFoundError as e:
        logger.error(e)
        sys.exit('Finishing.')
    except OSError as e:
        logger.error(e)
        sys.exit('Finishing.')
    
    # Configures page load timeout
    driver.set_page_load_timeout(settings['DRIVER']['page_load_timeout'])

    return driver
    
def choose_a_customer(settings):
    """Chooses a customer email from zip files placed in "zipped_userdata" folder.

    Parameters
    ----------
    settings : dict
        A dictionary with all settings to choose a customer.
    Return
    ------
    email : str
        The chosen email.
    """
    # Creates a logger
    logger = get_logger('choose_a_customer')

    all_zipfiles = get_files(settings['FOLDERS']['zipped_userdata'], extension='zip')
    if not all_zipfiles:
        logger.error('There is not zip files on %s.' % settings['FOLDERS']['zipped_userdata'])
        sys.exit('Finishing.')

    chosen_zipfile = choice(all_zipfiles)
    return splitext(basename(chosen_zipfile))[0]

def launch_robot(settings):
    """Launches the robot.
    
    Parameters
    ----------
    settings : dict
        A dictionary with all settings to launch a robot.
    """
    logger = get_logger('launch_robot')

    # Unzips user-data dir on temp folder if exists one
    try:
        unzip_user_data_dir(settings)
    except ZippedUserDataNotFoundError:
        logger.info('A new userdata directory will be created.')
   
    # Creates the browser
    driver = create_driver(settings)

    # Visits an IP verification page and wait a little.
    driver.get('https://ipapi.co/json')
    sleep(3)
    
    # Perform actions
    actions = Actions(driver, settings)
    return actions.run()

if __name__ == "__main__":
    pass