import sys
from pymongo import MongoClient
from common.utils import get_logger
from pymongo.errors import InvalidURI, OperationFailure
from common.errors import CustomerDataNotFoundOnCloudError


def get_client(username, password, cluster):
    """Gets personal_info and setting of customer from cloud.

    Parameters
    ----------
    username : str
        The username to connect on MongoDB - Atlas
    password : str
        The password to connect on MongoDB - Atlas
    cluster : str
        The cluster name on MongoDB - Atlas
    
    Return
    ------
    client : MongoClient
        The MongoClient instance to access MongoDb
    """
    # Creates a logger
    logger = get_logger('get_client')
    conn_string = (
        'mongodb+srv://%s:%s@%s.mongodb.net/test?retryWrites=true&w=majority' % (
            username,
            password,
            cluster
    ))
    try:
        client = MongoClient(conn_string)
    except InvalidURI:
        logger.error('An error occurred to access MongoDB Atlas. Please check if' +
                     ' the parameters are configured.')
        sys.exit('Finishing.')

    return client

def retrieve_customer_info(settings):
    """Retrives customer information from MongoDB instance
    
    Parameters
    ----------
    email : str
        The email of the customer.

    Return
    ------
    personal_info, settings : tuple
        A tuple with two dictionaries.
    """
    # Creates a logger
    logger = get_logger('retrieve_customer_info')
    # MongoDB client
    client = get_client(username=settings['MONGODB']['mdb_username'],
                        password=settings['MONGODB']['mdb_password'],
                        cluster=settings['MONGODB']['mdb_cluster'])   
    try:
        # MongoDB  database
        profiles = client.get_database(settings['MONGODB']['mdb_database'])
        # Retrieving data from collections
        personal_info = profiles.personal_info.find_one({'email': settings['email']})
        browser_settings = profiles.settings.find_one({'email': settings['email']})
    except OperationFailure as e:
        logger.error('MongoDB: %s' % e)
        sys.exit('Finishing.')

    # Check if the data exists
    if not personal_info:
        logger.error('There isn\'t personal_info data info for %s on MondoDB.' % settings['email'])
    if not browser_settings:
        logger.error('There isn\'t browser_settings data for %s on MondoDB.' % settings['email'])
    if personal_info and browser_settings:
        logger.info('personal_info and browser_settings found to %s on MongoDB.' % settings['email'])
    else:
        raise CustomerDataNotFoundOnCloudError(settings['email'])
    
    return personal_info, browser_settings