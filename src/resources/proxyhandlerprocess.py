import json
import time
import tzlocal
import requests
from pebble import ProcessPool
from json import JSONDecodeError
from datetime import datetime as dt
from multiprocessing import Process, Queue
from requests.exceptions import ProxyError
from concurrent.futures import TimeoutError


def search_dict_by_kv_from_list(data, key, value):
    """Searches a dict by key and value inside a list of dictionatiries.
    If more than one dictionary is founded, returns only the first one.

    Parameters
    ----------
    data : list
        A list of dictionaries to search inside
    key : string
        The key to compare
    value : string
        The value to search the dictionary
    
    Return
    ------
    _dict : dict
        The dictionary found. None if not
    """
    # Filter the list using the key and value
    filtered_data = list(filter(lambda x: x.get(key) == value, data))
    if filtered_data:
        # Return only the first dict
        _dict = filtered_data[0]
    else:
        _dict = None

    return _dict

def get_proxy_info(host=None, ip_api='http://ip-api.com/json'):
    """Gets information of a proxy from IP-API site.
    
    Parameters
    ----------
    host : string
        The <HOST> of the proxy service. Like: <PROXY_IP>:<PROXY_PORT>
    ip_api : string
        The site IP-API service. It is not mandatory.
    
    Returns
    -------
    connection : dict
        An object with information about connection.
    """
    # Creates the proxy object
    if host:
        http_proxy = {
            'http': 'http://%s' % host,
        }
    else:
        http_proxy = None
    # Creates a dict to store information
    connection = {'proxy': host,
                  'timestamp': dt.now(tz=tzlocal.get_localzone())}        
    # Makes a request to API thru the proxy
    try:
        # Finally retrieves from IP-API
        start_time = time.time()
        r = requests.get(ip_api, proxies=http_proxy)
        connection.update({'latency': time.time() - start_time})
        if r.status_code == 200:
            connection.update(json.loads(r.content))
        else:
            connection.update({'status': 'Not 200 status code'})
    except ProxyError as perror:
        connection.update({'status': 'ProxyError'})
    except JSONDecodeError as jerror:
        connection.update({'status': 'JSONDecodeError'})

    return connection

class ProxyHandlerProcess(Process):
    """ProxyHandler class is used to retrieve information from multiples
    proxies hosts - <IP>:<PORT>. It can be used to run in background.
    """

    def __init__(self,
                 queue,
                 hosts=[],
                 ip_api='http://ip-api.com/json',
                 response_timeout=5,
                 wait_time=15):
        """Initializes the ProxyHandler.
    
        Parameters
        ----------
        hosts : list
            A list containing hosts of the proxy service. For instance: 
            ['163.172.111.13:3403', '163.172.111.13:3404'].
        ip_api : string
            The site IP-API service to retrieve proxy information.
        response_timeout : int
            The time (in seconds) the proxy 
        """
        # Calls the init function of multiprocessing.Process
        Process.__init__(self)

        # Instance variables
        self.queue = queue
        self.hosts = hosts
        self.ip_api = ip_api
        self.response_timeout = response_timeout
        self.wait_time = wait_time
        # Stores proxies
        self.proxies = [{'proxy': h, 'timestamp':dt.now(tz=tzlocal.get_localzone())} for h in hosts]
    
    def clear_queue(self):
        """Clears the instance queue.
        """
        while not self.queue.empty():
            self.queue.get()

    def load_on_queue(self, data):
        """Loads data in the instance queue
        """
        while not self.queue.full():
            self.queue.put(data)

    def get_proxies(self):
        """Gets the proxies information in parallel

        Return
        ------
        proxies : list
            A list of dictionaries with proxies information
        """
        # List to store the proxies
        proxies= []
        # Creates a pool of processes
        with ProcessPool() as pool:
            future = pool.map(get_proxy_info,
                              self.hosts,
                              timeout=self.response_timeout)
            iterator = future.result()
            while True:
                try:
                    proxies.append(next(iterator))
                except StopIteration:
                    break
                except TimeoutError as error:
                    pass
                except Exception as error:
                    pass

        return proxies

    def update_proxies(self, updated_proxies):
        """Updates proxies instance variable.
        
        Parameters
        ----------
        updated_proxies : list
            A list with updated proxies.

        """
        for proxy in self.proxies:
            # Search proxy inside updated_proxies
            p = search_dict_by_kv_from_list(data=updated_proxies,
                                            key='proxy',
                                            value=proxy.get('proxy'))
            # If proxy was found
            if p:
                # Conditions to update
                is_success = p.get('status') == 'success' # Status proxy is success
                is_new_ip = p.get('query') != proxy.get('query') # A new IP is different from last check
                if is_success and is_new_ip:
                    msg = '{}: {} ({}/{}) -> {} ({}/{})'.format(proxy.get('proxy'),
                                                                proxy.get('query'),
                                                                proxy.get('city'),
                                                                proxy.get('regionName'),
                                                                p.get('query'),
                                                                p.get('city'),
                                                                p.get('regionName'))
                    print(msg)
                    # Update proxy
                    proxy.update(p)
            
    def run(self):
        """Overrides the run function of multiprocessing.Process
        """
        # Executes indefinitely
        while True:
            # Retrieves proxies information
            proxies = self.get_proxies()

            # Updates proxies instance variable
            self.update_proxies(proxies)

            # Empties the queue before stores new data.
            self.clear_queue()
            self.load_on_queue(proxies)

            # Wait a time to make another request
            time.sleep(self.wait_time)

        
if __name__ == '__main__':
    
    # Creates a list of proxies to retrieve information
    HOSTS = ['163.172.111.13:3403', '163.172.111.13:3404', '163.172.111.13:3405',
             '163.172.111.13:3406', '163.172.111.13:3407', '163.172.111.13:3408']
    
    # Creates a queue to stores the results.
    q = Queue(maxsize=1)
    # Creates process instance
    ph = ProxyHandlerProcess(hosts=HOSTS, queue=q)
    ph.start()
    # While ProxyHandler is alive, get info from queue
    while ph.is_alive():
        time.sleep(4)
        if not q.empty():
            # Gets proxies inside the queue
            proxies = q.get()
    
    # Waits for the proxy handler to complete
    ph.join()
