import argparse
from robots.launcher import initialize
from common.utils import read_config, check_asin


def parse_arguments():
    """Parses the user arguments passed by command-line.
    """
    # Main parser
    parser = argparse.ArgumentParser(description='Script to start the ShoppingRobot.',
                                     prog='python src\\start.py',
                                     usage='%(prog)s [options]')
    parser.set_defaults(func=initialize)
    parser.add_argument('control_file',
                        help='A CSV table with actions and customers to perform these actions. ' +
                             'To see a template of this file, open data folder.')
    parser.add_argument('-config_file',
                        dest='config_file',
                        default='settings/robot.ini',
                        help='The path of configuration INI file of the robot.'+
                             ' By default, the INI is robot.ini.')
    parser.add_argument('-detach',
                        dest='detach',
                        default=False,
                        action='store_true',
                        help='Detach browser from the terminal.')
    parser.add_argument('-k',
                        dest='keyword',
                        action='store',
                        help='The keyword used to search the products.')
    parser.add_argument('-a',
                        nargs='+',
                        dest='asins',
                        type=check_asin,
                        help='One or more ASIN codes.')
    parser.add_argument('-l',
                        dest='page_limit',
                        type=int,
                        default=21,
                        help='Stops the search when the page limit is reached. Default is 21')
    parser.add_argument('-pf',
                        dest='products_file',
                        default=None,
                        help='The path of products CSV file. It cannot be used ' +
                             'jointly with asins (-a) and keywords (-k) flags.')
    parser.add_argument('-i',
                        dest='iterations',
                        default=1,
                        type=int,
                        help='The number of iterations that the robot needs to do.')
    # Proxy options
    parser.add_argument('-proxy',
                        dest='proxy_service',
                        action='store',
                        choices=['ml', 'nn'],
                        help='Select a proxy for use. "ml" to microleaves and "nn" to netnut')
    parser.add_argument('-puser',
                        dest='proxy_user',
                        action='store',
                        help='Set a proxy user for proxies services ' +
                            'that requires authentication.')
    parser.add_argument('-ppwd',
                        dest='proxy_pwd',
                        action='store',
                        help='Set a proxy password for proxies ' +
                            'services that requires authentication.')
    parser.add_argument('-pip',
                        dest='proxy_server_ip',
                        help='If a Microleaves proxy service is selected, ' +
                        'you can specify a proxy server IP to connect.'+
                        ' By default, a random IP will be used.')
    parser.add_argument('-pport',
                        dest='proxy_server_port',
                        help='If a Microleaves proxy service is selected, ' +
                            'you can specify a proxy server port to connect.'+
                            ' By default, a random port will be used.')   
    # Instantiate the parser.
    args = parser.parse_args()
 
    # Argument verifications
    if bool(args.proxy_user) ^ bool(args.proxy_pwd):
        parser.error('proxy user (-puser) and proxy password (-ppwd) must ' +
                     'be given together.')
    
    if (args.proxy_service != 'ml' and 
        (bool(args.proxy_server_ip) or
         bool(args.proxy_server_port))):
        parser.error('proxy server IP (-pip) and proxy server port ' +
                     '(-pport) can be used only with Microleaves proxy ' +
                     ' service.')

    if bool(args.asins) ^ bool(args.keyword):
        parser.error('asins (-a) and keyword (-k) must be given together.')

    if bool(args.products_file):
        if bool(args.asins) or bool(args.keyword):
            parser.error('asins (-a) and/or keywords (-k) could not be used with '+
                         'products file (-pf).')

    return args

def main():
    """The initial method. Here, the parameters passed by the user by command-line
    will be converted in settings for the robot.
    """
    print('\n### ShoppingRobot ###\n')
    # Parses command-line arguments and convert into a dict
    cmd_arguments = parse_arguments()
    cmd_args_dict = {arg: getattr(cmd_arguments, arg) for arg in vars(cmd_arguments)}
    
    # Loads the .INI file and convert into a dict
    config = read_config(cmd_arguments.config_file)
    config_file_dict = {s: dict(config.items(s)) for s in config.sections()}

    # Joins both dicts to create the settings
    settings = {**cmd_args_dict, **config_file_dict}

    # Starts the procedures to launch the robot.
    cmd_arguments.func(settings)

if __name__ ==  '__main__':
    main()