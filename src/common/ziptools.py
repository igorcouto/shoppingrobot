import os
import sys
import zipfile
from common.utils import get_logger
from os.path import basename, abspath, join, exists
from common.errors import UserDataDirLockedError, ZippedUserDataNotFoundError


def unzip(file, dest_folder):
    """Unzips a .zip file in a destination folder.
    
    Parameters
    ----------
    file : str
        The .zip file to unzip
    dest_folder : str
        The folder to store the unzipped filed.
    """
    # Creates a logger
    logger = get_logger('unzip')

    try:
        filepath = abspath(file)
        _zip = zipfile.ZipFile(filepath)
        _zip.extractall(join(dest_folder))
        _zip.close()
    except OSError as e:
        logger.error(e)
        raise UserDataDirLockedError(basename(file))

def unzip_user_data_dir(settings):
    """Unzips the user-data directory in a temporary folder.

    Parameters
    ----------
    settings : dict
        A dictionary with all settings to unzips the user-data directory.
    """
    # Creates a logger
    logger = get_logger('unzip_user_data_dir')

    # Loads the folder to stored temporary files
    temp_folder = abspath(settings['FOLDERS']['temp'])
    if not exists(temp_folder):
        logger.info('A temporary folder will be created in %s.' % temp_folder)
        os.mkdir(temp_folder)
    
    # Where user-data directory will be stored.
    temp_userdata = abspath(join(temp_folder, settings['customer_info']['email']))
    logger.info('Temp user-data will be stored in %s' % temp_userdata)

    # Loads where the profiles are stored in
    zipped_userdata_folder = abspath(settings['FOLDERS']['zipped_userdata'])
    zipped_userdata = join(zipped_userdata_folder, settings['customer_info']['email']) + '.zip'
    if not exists(zipped_userdata):
        logger.error('%s not found in %s.' % (basename(zipped_userdata),
                                              zipped_userdata_folder))
        raise ZippedUserDataNotFoundError(settings['customer_info']['email'])
    else:
        logger.info('%s found. Unziping...' % basename(zipped_userdata))
        unzip(zipped_userdata, temp_folder)