import os
import re
import sys
import csv
import random
import string
import logging
import argparse
import configparser
from time import sleep
from shutil import copyfile
from os.path import exists, abspath, join
from random import choice, randint, random


def get_logger(name):
    """Creates a instance logger

    Return
    ------
    logger : logging.Logger
        A logger itself.
    """
    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)
    # Set a handler
    if not logger.hasHandlers():
        handler = logging.StreamHandler()
        handler.setLevel(logging.INFO)
        # Set a formatter
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(name)s - %(message)s')
        handler.setFormatter(formatter)
        # Add handler to logger
        logger.addHandler(handler)
    
    return logger

def read_config(file):
    """Reads .ini file from absolute path
    
    Parameters
    ----------
    file : str
        a .ini file

    Returns
    -------
    config : configparser.ConfigParser
        A ConfigParser object
    """
    file_path = os.path.abspath(file)
    config = configparser.ConfigParser(allow_no_value=True)
    config.read(file_path)
    return config

def is_original_driver(exe_path):
    """Checks if the executable is a original driver. Not edited.
    
    Parameters
    ----------
    exe_path : str
    
    Returns
    -------
    is_original : bool
        If True, the executable is original.
    """
    # Creates logger
    logger = get_logger('is_original_driver')

    # Absolute path
    exe_path = abspath(exe_path)
    
    content = None
    try:
        with open(exe_path, 'rb') as f:
            content = str(f.read())
    except FileNotFoundError as e:
        logger.error(e)
        sys.exit('Finishing.')

    is_original = content.find('$cdc_') != -1
    
    return is_original

def random_sleep(min=3, max=7):
    """Randomly waits a time in order to proceed. Use after robot
    performs an action.
    
    Parameters
    ----------
    min : int
        The minimum time that bot will sleep.
    max : int
        The maximum time that bot will sleep.
        
    """
    if max <= min:
        max = min + 3
    sleep(randint(min, max))

def get_files(path, extension=None):
    """Gets all files of a directory. To filter based on a extension, use
    extension parameters.
    
    Parameters
    ----------
    path : str
        The path of directory
    extension : str
        The extension to be filtered
    Return
    ------
    files : list
        A list of files
    """
    # Creates logger
    logger = get_logger('get_files')
    
    try:
        all_files = os.listdir(path)
    except FileNotFoundError as e:
        logger.error(e)
        sys.exit('Finishing.')
        
    if extension:
        regex = extension + '$'
        files = [join(path, f) for f in all_files if re.search(regex, f) is not None]
    else:
        files = [join(path, f) for f in all_files]
    return files

def check_asin(s, m=re.compile(r'^[a-zA-Z0-9]{10}$')):
    """Checks if a string matches with an ASIN code pattern.

    Parameters
    ----------
    s : string
        The string to be checked.
    
    Returns
    -------
    s : string
        The string itself. If the pattern was not found on the string, raises an ArgumentTypeError.
    """
    if not m.match(s):
        raise argparse.ArgumentTypeError('"%s" is not like an ASIN code.' % s)
    return s

def read_csv(file, delimiter=';', quotechar='"'):
    """Reads an CSV file and saves in a dictionary.
    """
    data = []
    
    with open(file) as f:
        reader = csv.DictReader(f, delimiter=delimiter, quotechar=quotechar)
        for r in reader:
            data.append(dict(r))
    
    return data

def split_and_strip(string, delimiter):
    """Split and strip a string by a delimiter
    
    Parameters
    ----------
    string : str
        The string to handle.
    delimiter : str
        The delimiter use to slit the string

    Returns
    -------
    list : list
        A list with split and stripped string
    """
    return [k.strip() for k in string.split(delimiter)]

def get_a_product(file):
    """From a CSV file, the function randomly chooses one product to use.
    
    Parameters
    ----------
    file : str
        The file that contains all products information.
    
    Returns
    -------
    product : dict
        A dict that contains information about the chosen product
    """

    products = []
    data = read_csv(file)
    selected_data = list(filter(lambda x: x['select'], data))
    
    for d in selected_data:
        status = d['status']
        asins = split_and_strip(d['asins'], ',')
        keywords = split_and_strip(d['keywords'], ',')
        weights_str = split_and_strip(d['weights'], ',')
        # Convert weights to float
        weights = [float(w) for w in weights_str]
        p = {'status': status,
             'asins': asins,
             'keywords': keywords,
             'weights': weights,}
        products.append(p)        

    product = choice(products)

    return product

def weighted_choice(sequence, weights):
    """Receives a sequence and its weights to choose one element of the sequence.
    - Sequence and weights must be the same lenght
    - Sum of weights need be 1
    
    Parameters
    ----------
    sequence : list
        A sequence
    weights : list

    Returns
    -------
    element : str
        A element of the sequence
    """
    weights = [float(w) for w in weights]
    assert len(weights) == len(sequence), 'Keywords and weights must be same lenght'
    assert abs(1. - sum(weights)) < 1e-6, 'Sum of weights need be 1'
    x = random()
    for i, elmt in enumerate(sequence):
        if x <= weights[i]:
            return elmt
        x -= weights[i]

def get_a_customer(file):
    """Choose randomically a customer from a CSV file
    
    Parameters
    ----------
    file : str
        A file path with user agents information. This file must be a CSV file
        type, separated with semicolon and using double quotes '"'.
    
    Returns
    -------
    customer : dict
        A dictionary with information about the chosen customer.
    """
    customers = []
    data = read_csv(file)
    selected_data = list(filter(lambda x: x['select'], data))
    
    for d in selected_data:
        # Remove unuseful column
        del d['select']
        # Convert variables with boolean values
        bool_cols = ['empty_cart', 'add_to_wish_list', 'purchase_the_first']
        for col in bool_cols:
            d[col] = bool(d[col])
        
        customers.append(d)
    
    customer = choice(customers)

    return customer

def update_product(settings):
    """If necessary, updates product.

    Parameters
    ----------
    settings : dict
        A dictionary with all settings to initialize.

    Return
    ------
    settings : dict
        Updated settings.
    """
    # Processes the products file if informed
    if settings.get('products_file'):
        # Select a product
        product = get_a_product(settings.get('products_file'))
        # Update setings
        settings['asins'] = product['asins']
        settings['keyword'] = weighted_choice(sequence=product['keywords'],
                                              weights=product['weights'])
        settings['status'] = product['status']
    
    return settings

def update_customer(settings):
    """If necessary, updates customer data on settings.

    Parameters
    ----------
    settings : dict
        A dictionary with all settings to initialize.

    Return
    ------
    settings : dict
        Updated settings.
    """
    # Select a customer
    customer = get_a_customer(settings.get('control_file'))
    settings['customer_info'] = customer

    return settings

if __name__ == "__main__":
    pass