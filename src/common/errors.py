from common.utils import get_logger


class AccountProblemError(Exception):
    """Raised when an Account has a problem.
    """
    def __init__(self, email, msg=None):
        # Creates a logger
        logger = get_logger('AccountProblemError')
        logger.error('There was a problem with %s account.' % email)
        if msg:
            logger.error(msg)

class UserDataDirLockedError(Exception):
    """Raised when it is not possible open user data dir.
    """
    def __init__(self, dir):
        # Creates a logger
        logger = get_logger('UserDataDirLockedError')
        logger.error('It\'s not possible open userdata directory: %s' % dir)

class CustomerDataNotFoundOnCloudError(Exception):
    """Raised when a customer data is not found on cloud.
    """
    def __init__(self, email):
        # Creates a logger
        logger = get_logger('CustomerDataNotFoundOnCloudError')
        logger.error('Customer data (%s) not found on cloud.' % email)

class ZippedUserDataNotFoundError(Exception):
    """Raised when there isn't a zipped user-data.
    """
    def __init__(self, email):
        # Creates a logger
        logger = get_logger('ZippedUserDataNotFoundError')
        logger.error('Zipped user-data (%s) dir not found.' % email)